#include <iostream>
#include <eigen3/Eigen/Dense>
#include <ctime>
#include <cmath>
#include <cassert>
#include "chrono.hpp"

Eigen::MatrixXd matProduct(const Eigen::MatrixXd &mat1, const Eigen::MatrixXd &mat2)
{
  assert(mat1.cols() == mat2.rows() && "error: matrix product -> impossible size");

  Eigen::MatrixXd res(mat1.rows(), mat2.cols());

  for ( size_t i = 0; i < size_t( res.rows() ); ++i ) {
    for ( size_t j = 0; j < size_t( res.cols() ); ++j ) {

      res(i,j) = 0;

      //for ( int L = 0;  L < mat1.cols(); ++L) {
      //  for ( int C = 0; C < mat1.cols(); ++C ) {
      //    res(i, j) += mat1(i, C) * mat2(L, j);
      //  }
      //}
      for ( int k = 0;  k< mat1.cols(); ++k ) {
        res(i, j) += mat1(i, k) * mat2(k, j);
      }
      //for ( int k = 0; k < mat1.cols(); ++k ) {
      //  res(i, j) += mat1(i, j+k) * mat2(i+k, j);
      //}
    }
  }

  return res;
}

int main( int argc, char* argv[] )
{
  //const size_t dimension = 5;
  Eigen::MatrixXd mat1 = Eigen::MatrixXd::Random(atoi( argv[1] ), atoi( argv[3] ));
  Eigen::MatrixXd mat2 = Eigen::MatrixXd::Random(atoi( argv[3] ), atoi( argv[4] ));

  Eigen::MatrixXd res1, res2;
  TP_CPP_IMAC2::Chrono chrono;
  chrono.start();
  res1 = matProduct(mat1, mat2);
  chrono.stop();

  std::cout << "res1 = " << res1 << std::endl << ", time spent = "<< chrono.timeSpan() << std::endl;

  chrono.start();
  res2 = mat1 * mat2;
  chrono.stop();

  std::cout << "res2 = " << res2 << std::endl << ", time spent = "<< chrono.timeSpan() << std::endl;

  std::cout << (res1 - res2).norm() << std::endl;

  return 0;
}

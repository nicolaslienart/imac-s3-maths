#include <eigen3/Eigen/Dense>
#include "chrono.hpp"
#include <iostream>

Eigen::MatrixXd matProduct(const Eigen::MatrixXd &mat1, const Eigen::MatrixXd &mat2)
{
  Eigen::MatrixXd res(mat1.rows(), mat2.cols());

  for ( size_t i = 0; i < size_t( res.rows() ); ++i ) {
    for ( size_t j = 0; j < size_t( res.cols() ); ++j ) {
      res(i,j) = 0;
      for ( int k = 0;  k< mat1.cols(); ++k ) {
        res(i, j) += mat1(i, k) * mat2(k, j);
      }
    }
  }

  return res;
}

Eigen::MatrixXd strassen(const Eigen::MatrixXd& A, const Eigen::MatrixXd& B)
{
  if ( A.cols() <= 128 ) {
    return A * B;
  }
  else {
    Eigen::MatrixXd a = A.topLeftCorner(A.cols()/2, A.rows()/2);
    Eigen::MatrixXd b = A.topRightCorner(A.cols()/2, A.rows()/2);
    Eigen::MatrixXd c = A.bottomLeftCorner(A.cols()/2, A.rows()/2);
    Eigen::MatrixXd d = A.bottomRightCorner(A.cols()/2, A.rows()/2);

    Eigen::MatrixXd e = B.topLeftCorner(B.cols()/2, B.rows()/2);
    Eigen::MatrixXd f = B.topRightCorner(B.cols()/2, B.rows()/2);
    Eigen::MatrixXd g = B.bottomLeftCorner(B.cols()/2, B.rows()/2);
    Eigen::MatrixXd h = B.bottomRightCorner(B.cols()/2, B.rows()/2);

    Eigen::MatrixXd P1 = strassen(a, f-h);
    Eigen::MatrixXd P2 = strassen(a+b, h);
    Eigen::MatrixXd P3 = strassen(c+d, e);
    Eigen::MatrixXd P4 = strassen(d, g-e);
    Eigen::MatrixXd P5 = strassen(a+d, e+h);
    Eigen::MatrixXd P6 = strassen(b-d, g+h);
    Eigen::MatrixXd P7 = strassen(a-c, e+f);

    Eigen::MatrixXd r = P5 + P4 - P2 + P6;
    Eigen::MatrixXd s = P1 + P2;
    Eigen::MatrixXd t = P3 + P4;
    Eigen::MatrixXd u = P1 + P5 - P3 - P7;

    Eigen::MatrixXd res(A.cols(), A.rows());

    res.topLeftCorner(res.cols()/2, res.rows()/2) = r;
    res.topRightCorner(res.cols()/2, res.rows()/2) = s;
    res.bottomLeftCorner(res.cols()/2, res.rows()/2) = t;
    res.bottomRightCorner(res.cols()/2, res.rows()/2) = u;

    return res;
  }
}

int main( void )
{
  Eigen::MatrixXd A = Eigen::MatrixXd::Random(2048,2048);	
  Eigen::MatrixXd B = Eigen::MatrixXd::Random(2048,2048);	

  TP_CPP_IMAC2::Chrono chrono;

  chrono.start();
  Eigen::MatrixXd Res3 = matProduct(A, B);
  chrono.stop();
  std::cout << "Time spent = "<< chrono.timeSpan() << std::endl;

  chrono.start();
  Eigen::MatrixXd Res2 = strassen(A, B);
  chrono.stop();
  std::cout << "Time spent = "<< chrono.timeSpan() << std::endl;

  chrono.start();
  Eigen::MatrixXd Res1 =  A * B;
  chrono.stop();
  std::cout << "Time spent = "<< chrono.timeSpan() << std::endl;

  Eigen::setNbThreads(2);
  chrono.start();
  Eigen::MatrixXd Res4 = A * B;
  chrono.stop();
  std::cout << "Time spent = "<< chrono.timeSpan() << std::endl;


  /*
  if (Res1 == Res2) {
    std::cout << "Ouaii" << std::endl;
  }

  if (((Res1 - Res2).norm())) {
    std::cout << "Ouaii lol" << std::endl;
  }
  */

  return 0;
}

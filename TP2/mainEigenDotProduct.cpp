#include <iostream>
#include <eigen3/Eigen/Dense>
#include <ctime>
#include <cmath>
#include <cassert>
#include "chrono.hpp"

double dotProduct(const Eigen::VectorXd &vec1, const Eigen::VectorXd &vec2)
{
  assert(vec1.size() == vec2.size() && "error: dot -> impossible size");

  double res = 0;

  for ( size_t i = 0; i < size_t( vec1.size() ); ++i ) {
    res += vec1[i]*vec2[i];
  }
  return res;
}

int main( void )
{
  const size_t dimension = 10000;
  Eigen::VectorXd vec1 = Eigen::VectorXd::Random(dimension);
  Eigen::VectorXd vec2 = Eigen::VectorXd::Random(dimension);

  double res1, res2;
  TP_CPP_IMAC2::Chrono chrono;
  chrono.start();
  res1 = dotProduct(vec1, vec2);
  chrono.stop();

  std::cout << "res1 = " << res1 << ", time spent = "<< chrono.timeSpan() << std::endl;

  chrono.start();
  res2 =vec1.dot(vec2);
  chrono.stop();

  std::cout << "res2 = " << res2 << ", time spent = "<< chrono.timeSpan() << std::endl;

  std::cout <<  std::abs(res1-res2) << std::endl;

  return 0;
}

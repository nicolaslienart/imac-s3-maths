#include <iostream>
#include <iomanip>

int main( void )
{
  double number;
  std::cout << "Enter a number: ";
  std::cin >> number;

  double test = number;
  double epsilon = 0.00000001;
  double power = test*test;
  double step;
  while ( power < ( number-epsilon ) || power > ( number+epsilon )) {
    if ( power > (number-epsilon) ) {
      step = test/2;
      test /= 2;
    }
    else {
      step/=2;
      test += step;
    }
    power = test*test;
    
    std::cout << std::setprecision(20) << test << "² != "<< power << std::endl;
  }

  std::cout << std::endl;

  std::cout << "√"<<number<<"="<< test << std::endl;
  return 0;
}

#include <iostream>
#include <vector>
#include <cmath>

/*
float ecartType_On2(const std::vector<float> vec) {

}
*/

float ecartType_On(const std::vector<float>& vec) {
  float alpha = 0;
  for ( int i = 0; i < int( vec.size() ); ++i ) {
    alpha += 1.f/int( vec.size() )*vec[i];
  }

  float sum2res = 0;
  for ( int i = 0; i < int(vec.size()); ++i ) {
    sum2res += vec[i];
  }

  alpha -= 1.f/std::pow( int(vec.size()), 2 )*std::pow(sum2res, 2);

  return alpha;
}

int main( void )
{
  std::vector<float> vec;

  int i = 0;
  while(true) {
    float userIn;
    std::cout << "vec[" << i <<"] = ";
    std::cin >> userIn;
    if ( userIn == *(int *)&"inf") {
      break;
    }
    else {
      vec.push_back(userIn);
    }
  }

  std::cout << ecartType_On(vec) << std::endl;

  return 0;
}

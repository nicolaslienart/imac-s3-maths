#include <cmath>
#include <iostream>

int main()
{
  float a = 5.0f;
  float b = 5.0f;

  if (a == b) {
    std::cout << "a == b" << std::endl;
  }

  /**
   * if (|a-b| < Epsilon);
   * if (|a-b|/|a| < Epsilon);
   * */


  int c = 1;
  // Erreur grave c/3.f
  float d = c/3;
  std::cout << d << std::endl;

  // e = -inf
  float e = -5.0/0;
  std::cout << "e = " << e << std::endl;

  float f = std::powf(2.0,  40);
  std::cout << "f = " << f << std::endl;
  int g = f;
  std::cout << "g = " << g << std::endl;
  return 0;
}

#include <iostream>

double suiteNextd(const int n) 
{
  if ( n == 0 ) {
    return 1.0/3.0;
  }
  return 4 * suiteNextd(n-1) - 1;
}

float suiteNextf(const int n) 
{
  if ( n == 0 ) {
    return 1.f/3.f;
  }
  return 4 * suiteNextf(n-1) - 1;
}

int main()
{
  std::cout << suiteNextf(100) << std::endl;
  std::cout << suiteNextd(550) << std::endl;
  return 0;
}

#include <iostream>
#include <cstdlib>


void printUchar(const unsigned char x)
{
  std :: cout << (int ) x << " : "; // NOTE: demande de type spécifique (casting)

  std::cout << std::endl;
  std::cout << std::endl;
}


int main(int argc, char **argv)
{
  if(argc != 2){
    std::cerr << "usage: " << argv[0] << " nb" << std::endl;
    return 0;
  }
  printUchar(atoi(argv[1]));

  return 1;
}

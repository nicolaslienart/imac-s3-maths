#include <iomanip> 
#include <iostream>
#include <cmath>

int main(void)
{
  double a, res;
  std::cout << "nb : ";
  std::cin >> a;

  bool run = true;
  double i = 1.0;
  double step = 1.0;
  double power;
  while(run) {
    i += step;
    
    power = std::pow(i, 2);
    std::cout << "i="<<i << std::endl;

    if (power > (a-0.001) && power < (a+0.001)) {
      run = false;
      res = i;
    }
    else if (power > a) {
      step = - std::abs(step/2.0);
      std::cout << "step" << std::setprecision(20) << step << std::endl;
    }
    else {
      step = std::abs(step);
      i += step;
    }
  }

  std::cout << "res = " << res << std::endl;
  return 0;
}

#include <iostream>
#include <iomanip>

double newtonSqrt(const double number, const int N) {
  if ( N==0 ) {
    return 1.0;
  }
  return (newtonSqrt(number, N-1) + number/newtonSqrt(number, N-1))/2.0;
}

int main( void )
{
  double number;
  std::cout << "number: ";
  std::cin >> number;
  int N = 25;

  double sqrt = newtonSqrt(number, N);
  std::cout << "√"<<number<< "=" << std::setprecision(20) << sqrt << std::endl;
  return 0;
}

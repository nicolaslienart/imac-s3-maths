#include <iostream>
#include <cstdlib>
#include <string>

void printUchar(const unsigned char x)
{
  std :: cout << (int ) x << " : ";
  std::string binary;
  int reste = x;
  for (int i = 128; i >= 1; i/=2) {
    if (i == 128 / 16) {
      binary += " ";
    }
    if (reste / i) {
      binary += "1";
    }
    else {
      binary += "0";
    }
    reste %= i;
  }

  std::cout << binary << std::endl;
}


int main(int argc, char **argv)
{
  if(argc != 2){
    std::cerr << "usage: " << argv[0] << " nb" << std::endl;
    return 0;
  }
  printUchar(atoi(argv[1]));

  return 0;
}

#include <iostream>

float bale(const int i) {
  if ( i==1 ) {
    return 1;
  }
  return 1.f/( i*i ) + bale(i-1);
}

int main( void )
{
  float u = 0;
  int N = 20;
  for ( int i = 1; i < N; ++i ) {
    u += 1.f/( i*i );
  }

  std::cout << "u=" << u << std::endl;

  std::cout << "u=" << bale(20) << std::endl;
  return 0;
}
